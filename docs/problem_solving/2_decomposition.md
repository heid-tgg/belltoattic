@startuml

card level_1 [
    How can a resident be notified when a guest is arriving?
    ] 


card level_2_tech_bell_signal [
    What is the signal one gets from the external bell system?
]

card level_2_notification [
    What is the signal the resident can notify?
]

card level_2_read_signal [
    How can one read the signal of the bell?
]

card level_3_read_signal_hardware [
    What hardeware to use for the listen to the signal?
]

card level_3_read_signal_software [
    What software to use for the listen to the signal?
]

level_1 --> level_2_notification 
level_1 --> level_2_tech_bell_signal
level_2_tech_bell_signal -->level_3_read_signal_hardware
level_2_tech_bell_signal -->level_3_read_signal_software

@enduml